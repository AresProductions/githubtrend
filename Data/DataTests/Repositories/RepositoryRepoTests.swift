//
//  RepositoryRepoTests.swift
//  DataTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
@testable import Data
import Domain
import Foundation
import Nimble
import XCTest

// swiftlint:disable force_try
// swiftlint:disable force_cast
class RepositoryRepoTests: XCTestCase {
    var cancellables = [AnyCancellable]()

    func testSyncTrendingReturnsVoidGivenDataSourcesSucceed() async {
        // given
        let mockedRepositories = [anyRepository(), anyRepository(), anyRepository()]
        let mockLocalSource = MockRepositoryLocalSource(expectedCreateOrUpdareResult: .success(()))
        let mockRemoteSource = MockRepositoryRemoteSource(expectedGetResult: .success(mockedRepositories))
        let repo = RepositoryRepo(localSource: mockLocalSource, remoteSource: mockRemoteSource)

        // when
        let result: Void = try! await repo.syncTrending().get()

        // then
        await expect(result).to(beVoid())
        await expect(mockLocalSource.createOrUpdareSpy[0]).to(equal(mockedRepositories))
        await expect(mockLocalSource.createOrUpdareSpy.count).to(equal(1))
        await expect(mockRemoteSource.getTrendingSpy.count).to(equal(1))
    }

    func testSyncTrendingReturnsErrorGivenRemoteSourceReturnsError() async {
        // given
        let mockError = DomainError.basic(description: "remote error")
        let mockLocalSource = MockRepositoryLocalSource()
        let mockRemoteSource = MockRepositoryRemoteSource(expectedGetResult: .failure(mockError))
        let repo = RepositoryRepo(localSource: mockLocalSource, remoteSource: mockRemoteSource)

        do {
            // when
            _ = try await repo.syncTrending().get()
            XCTFail("should throw error")
        } catch {
            // then
            let receivedError = error as! DomainError
            await expect(receivedError).to(equal(mockError))
            await expect(mockRemoteSource.getTrendingSpy.count).to(equal(1))
            await expect(mockLocalSource.createOrUpdareSpy.count).to(equal(0))
        }
    }

    func testSyncTrendingReturnsErrorGivenLocalSourceReturnsError() async {
        // given
        let mockedRepositories = [anyRepository(), anyRepository(), anyRepository()]
        let mockError = DomainError.basic(description: "local error")
        let mockLocalSource = MockRepositoryLocalSource(expectedCreateOrUpdareResult: .failure(mockError))
        let mockRemoteSource = MockRepositoryRemoteSource(expectedGetResult: .success(mockedRepositories))
        let repo = RepositoryRepo(localSource: mockLocalSource, remoteSource: mockRemoteSource)

        do {
            // when
            _ = try await repo.syncTrending().get()
            XCTFail("should throw error")
        } catch {
            // then
            let receivedError = error as! DomainError
            await expect(receivedError).to(equal(mockError))
            await expect(mockRemoteSource.getTrendingSpy.count).to(equal(1))
            await expect(mockLocalSource.createOrUpdareSpy.count).to(equal(1))
        }
    }

    func testObserveTrendingReturnsRepositoriesGivenLocalSourceProvidesData() {
        // given
        let repositories = [anyRepository(), anyRepository(), anyRepository()]
        let mockLocalSource = MockRepositoryLocalSource(expectedRepositories: repositories)
        let mockRemoteSource = MockRepositoryRemoteSource()
        let repo = RepositoryRepo(localSource: mockLocalSource, remoteSource: mockRemoteSource)

        // when
        let expectation = XCTestExpectation(description: "received")
        var receivedRepositories: [Repository]?
        repo.observeTrending().sink { repositories in
            receivedRepositories = repositories
            expectation.fulfill()
        }.store(in: &cancellables)

        // then
        wait(for: [expectation], timeout: 3)
        expect(repositories).to(equal(receivedRepositories))
        expect(mockLocalSource.observeTrendingSpy.count).to(equal(1))
        expect(mockRemoteSource.getTrendingSpy.count).to(equal(0))
    }

    func testObserveReturnsRepositoryGivenLocalSourceProvidesData() {
        // given
        let repository = anyRepository()
        let mockLocalSource = MockRepositoryLocalSource(expectedRepository: repository)
        let mockRemoteSource = MockRepositoryRemoteSource()
        let repo = RepositoryRepo(localSource: mockLocalSource, remoteSource: mockRemoteSource)

        // when
        let expectation = XCTestExpectation(description: "received")
        var receivedRepository: Repository?
        repo.observe(url: repository.url).sink { repository in
            receivedRepository = repository
            expectation.fulfill()
        }.store(in: &cancellables)

        // then
        wait(for: [expectation], timeout: 3)
        expect(repository).to(equal(receivedRepository))
        expect(mockLocalSource.observeSpy.count).to(equal(1))
        expect(mockLocalSource.observeSpy[0]).to(equal(repository.url))
    }
}
