//
//  MockRepositoryLocalSource.swift
//  DataTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
@testable import Data
import Domain
import Foundation

class MockRepositoryLocalSource: IRepositoryLocalSource {
    let expectedCreateOrUpdareResult: Result<Void, DomainError>
    let expectedRepositories: [Repository]
    let expectedRepository: Repository?

    var createOrUpdareSpy: [[Repository]] = []
    var observeTrendingSpy: [Void] = []
    var observeSpy: [String] = []

    init(expectedCreateOrUpdareResult: Result<Void, DomainError> = .success(()),
         expectedRepositories: [Repository] = [],
         expectedRepository: Repository? = nil) {
        self.expectedCreateOrUpdareResult = expectedCreateOrUpdareResult
        self.expectedRepositories = expectedRepositories
        self.expectedRepository = expectedRepository
    }

    func createOrUpdate(repositories: [Repository]) async -> Result<Void, DomainError> {
        createOrUpdareSpy.append(repositories)
        return expectedCreateOrUpdareResult
    }

    func observeTrending() -> AnyPublisher<[Repository], Never> {
        observeTrendingSpy.append(())
        return Just(expectedRepositories).eraseToAnyPublisher()
    }

    func observe(url: String) -> AnyPublisher<Domain.Repository?, Never> {
        observeSpy.append(url)
        return Just(expectedRepository).eraseToAnyPublisher()
    }
}
