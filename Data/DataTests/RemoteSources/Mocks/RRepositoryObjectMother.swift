//
//  RRepositoryObjectMother.swift
//  DataTests
//
//  Created by Ares Ceka on 29/10/22.
//

@testable import Data
import Domain
import Foundation

public func anyRRepository() -> RRepository {
    RRepository(
        author: randomString(),
        name: randomString(),
        avatar: randomString(),
        url: randomString(),
        description: randomString(),
        language: randomString(),
        languageColor: randomString(),
        stars: randomInt(),
        forks: randomInt(),
        currentPeriodStars: randomInt(),
        builtBy: [anyRContributor(), anyRContributor(), anyRContributor()])
}
