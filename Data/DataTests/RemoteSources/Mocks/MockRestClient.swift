//
//  MockRestClient.swift
//  DataTests
//
//  Created by Ares Ceka on 29/10/22.
//

import Combine
@testable import Data
import Domain
import Foundation

// swiftlint:disable force_cast
class MockRestClient: IRestClient {
    let expectedGetResult: Result<Decodable, RestClientError>

    var getEndpointSpy: [Endpoint] = []

    init(expectedGetResult: Result<Decodable, RestClientError> = .failure(RestClientError.invalidResponse(code: 404))) {
        self.expectedGetResult = expectedGetResult
    }

    func get<T, E>(_ endpoint: E) async -> Result<T, RestClientError> where T: Decodable, E: Endpoint {
        getEndpointSpy.append(endpoint)
        return expectedGetResult.map { decodable in
            decodable as! T // force cast because of compiler issues
        }
    }
}
