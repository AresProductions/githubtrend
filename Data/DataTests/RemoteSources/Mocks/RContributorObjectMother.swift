//
//  RContributorObjectMother.swift
//  DataTests
//
//  Created by Ares Ceka on 29/10/22.
//

@testable import Data
import Domain
import Foundation

#if DEBUG

import Foundation

func anyRContributor() -> RContributor {
    RContributor(username: randomString(), href: randomString(), avatar: randomString())
}

#endif
