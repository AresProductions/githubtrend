//
//  MockRepositoryRemoteSource.swift
//  DataTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
@testable import Data
import Domain
import Foundation

class MockRepositoryRemoteSource: IRepositoryRemoteSource {
    let expectedGetResult: Result<[Repository], DomainError>

    var getTrendingSpy: [Void] = []

    init(expectedGetResult: Result<[Repository], DomainError> = .success([])) {
        self.expectedGetResult = expectedGetResult
    }

    func getTrending() async -> Result<[Repository], DomainError> {
        getTrendingSpy.append(())
        return expectedGetResult
    }
}
