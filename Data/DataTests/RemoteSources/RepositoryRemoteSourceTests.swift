//
//  RepositoryRemoteSourceTests.swift
//  DataTests
//
//  Created by Ares Ceka on 29/10/22.
//

import Combine
@testable import Data
import Domain
import Foundation
import Nimble
import XCTest

// swiftlint:disable force_try
// swiftlint:disable force_cast
class RepositoryRemoteSourceTests: XCTestCase {
    func testSuccessGivenRestClientReturnsArrayOfRepositories() async {
        // given
        let rRepositories = [anyRRepository(), anyRRepository(), anyRRepository(), anyRRepository()]
        let mockRestClient = MockRestClient(expectedGetResult: .success(rRepositories))
        let remoteSource = RepositoryRemoteSource(restClient: mockRestClient)

        // when
        let trendingResult = try! await remoteSource.getTrending().get()
        let expectedTrendingResult = rRepositories.map { $0.mapToDomain() }

        // then
        await expect(trendingResult).to(equal(expectedTrendingResult))
        await expect(mockRestClient.getEndpointSpy.count).to(equal(1))
        await expect(mockRestClient.getEndpointSpy[0].url).to(equal(GithubEndpoint.trending.url))
    }

    func testErrorGivenRestClientReturnsRestClientError() async {
        // given
        let mockError = RestClientError.invalidResponse(code: 404)
        let mockRestClient = MockRestClient(expectedGetResult: .failure(mockError))
        let remoteSource = RepositoryRemoteSource(restClient: mockRestClient)

        do {
            // when
            _ = try await remoteSource.getTrending().get()
            XCTFail("should throw error")
        } catch {
            // then
            let expectedError = mockError.mapToDomainError()
            let errorReturned = error as! DomainError
            await expect(errorReturned).to(equal(expectedError))
            await expect(mockRestClient.getEndpointSpy[0].url).to(equal(GithubEndpoint.trending.url))
            await expect(mockRestClient.getEndpointSpy.count).to(equal(1))
        }
    }
}
