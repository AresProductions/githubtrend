//
//  RepositoryLocalSourceTests.swift
//  DataTests
//
//  Created by Ares Ceka on 29/10/22.
//

import Combine
import CoreData
@testable import Data
import Domain
import Foundation
import Nimble
import XCTest

 class RepositoryLocalSourceTests: XCTestCase {
    var cancellables = [AnyCancellable]()

    func testSaveNewRepositoriesAndRetrieveThemSortedByName() async {
        // given
        let container = NSPersistentContainer.createDatabase(testing: false)
        let repositories = [anyRepository(), anyRepository(), anyRepository()]
        let localSource = RepositoryLocalSource(container: container)

        // when
        let expectation = XCTestExpectation(description: "received")
        _ = await localSource.createOrUpdate(repositories: repositories)
        var receivedRepositories: [Repository]?
        localSource.observeTrending().sink { repositories in
            receivedRepositories = repositories
            expectation.fulfill()
        }.store(in: &cancellables)

        // then
        let expectedSortedRepositories = repositories.sorted(by: { repoA, repoB in
            repoA.name.caseInsensitiveCompare(repoB.name) == .orderedAscending
        })
        wait(for: [expectation], timeout: 3)
        await expect(receivedRepositories).to(equal(expectedSortedRepositories))
    }

    func testReplaceExistingRepositoriesWithNew() async {
        // given
        let container = NSPersistentContainer.createDatabase(testing: true)
        let existingRepositories = [anyRepository(), anyRepository(), anyRepository()]
        let localSource = RepositoryLocalSource(container: container)

        // add some existing repositories
        var expectation = XCTestExpectation(description: "received")
        _ = await localSource.createOrUpdate(repositories: existingRepositories)
        var receivedRepositories: [Repository]?
        localSource.observeTrending().sink { repositories in
            receivedRepositories = repositories
            if !repositories.isEmpty {
                expectation.fulfill()
            }
        }.store(in: &cancellables)

        // check if savedRepositories state is correct
        let expectedSortedRepositories = existingRepositories.sorted(by: { repoA, repoB in
            repoA.name.caseInsensitiveCompare(repoB.name) == .orderedAscending
        })
        wait(for: [expectation], timeout: 3)
        await expect(receivedRepositories).to(equal(expectedSortedRepositories))

        // when saving new repositories
        expectation = XCTestExpectation(description: "new received")
        let newRepositories = [anyRepository(), anyRepository()].sorted(by: { repoA, repoB in
            repoA.name.caseInsensitiveCompare(repoB.name) == .orderedAscending
        })
        _ = await localSource.createOrUpdate(repositories: newRepositories)

        // then check if savedRepositories have been replaced with new repositories
        wait(for: [expectation], timeout: 3)
        await expect(receivedRepositories).to(equal(newRepositories))
    }

     func testObserveRepositoryGivenStoredInDatabase() async {
         // given
         let container = NSPersistentContainer.createDatabase(testing: false)
         let storedRepository = anyRepository()
         let repositories = [storedRepository]
         let localSource = RepositoryLocalSource(container: container)

         // when
         let expectation = XCTestExpectation(description: "received")
         _ = await localSource.createOrUpdate(repositories: repositories)
         var receivedRepository: Repository?
         localSource.observe(url: storedRepository.url).sink { repository in
             receivedRepository = repository
             expectation.fulfill()
         }.store(in: &cancellables)

         // then
         wait(for: [expectation], timeout: 3)
         await expect(receivedRepository).to(equal(storedRepository))
     }

     func testObserveRepositoryGivenNotStoredInDatabase() {
         // given
         let container = NSPersistentContainer.createDatabase(testing: false)
         let localSource = RepositoryLocalSource(container: container)
         let url = randomString()

         // when
         let expectation = XCTestExpectation(description: "received")
         var receivedRepository: Repository?
         localSource.observe(url: url).sink { repository in
             receivedRepository = repository
             expectation.fulfill()
         }.store(in: &cancellables)

         // then
         wait(for: [expectation], timeout: 3)
         expect(receivedRepository).to(beNil())
     }

 }
