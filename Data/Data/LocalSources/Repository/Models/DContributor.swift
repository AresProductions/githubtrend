//
//  DContributor.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import CoreData
import Domain
import Foundation

extension DContributor: SynchronizableManagedObject {
    typealias OtherObject = Contributor

    func updateValues(using object: Contributor) {
        self.username = object.username
        self.profileUrl = object.profileURL
        self.avaterImageUrl = object.avatarImageURL
    }
}

extension DContributor {
    func mapToDomain() -> Contributor {
        Contributor(username: self.username ?? "",
                    profileURL: self.profileUrl ?? "",
                    avatarImageURL: self.avaterImageUrl ?? "")
    }
}
