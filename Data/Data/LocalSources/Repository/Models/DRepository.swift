//
//  DRepository.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import CoreData
import Domain
import Foundation

extension DRepository: SynchronizableManagedObject {
    typealias OtherObject = Repository

    func updateValues(using object: Repository) {
        self.url = object.url
        self.name = object.name
        self.stars = Int64(object.stars)
        self.repoDescription = object.description
        self.language = object.language
        self.languageColor = object.languageColorHexCode
        self.forks = Int64(object.forks)
        self.avatarImageUrl = object.avatarImageURL
        self.author = object.author
        self.updateContributors(contributors: object.contributors, context: managedObjectContext)
    }

    private func updateContributors(contributors: [Contributor], context: NSManagedObjectContext?) {
        if let context = context {
            let dContributors = contributors.map { contributor in
                let dContributor = DContributor(context: context)
                dContributor.updateValues(using: contributor)
                return dContributor
            }

            self.contributors = NSOrderedSet(array: dContributors.sorted(by: { contributorA, contributorB in
                contributorA.username ?? "" < contributorB.username ?? ""
            }))
        }
    }
}

extension DRepository {
    static func fetchRequest(for url: String) -> NSFetchRequest<DRepository> {
        let request: NSFetchRequest<DRepository> = DRepository.fetchRequest()
        request.predicate = NSPredicate(format: "url == %@", url)
        return request
    }
}

extension DRepository {
    func mapToDomain() -> Repository {
        // swiftlint:disable:next force_cast
        let dContributors = self.contributors?.array as! [DContributor]
        return Repository(name: self.name ?? "",
                          author: self.author ?? "",
                          avatarImageURL: self.avatarImageUrl ?? "",
                          url: self.url ?? "",
                          description: self.repoDescription ?? "",
                          language: self.language ?? "",
                          languageColorHexCode: self.languageColor ?? "",
                          stars: Int(self.stars),
                          forks: Int(self.forks),
                          contributors: dContributors.map { $0.mapToDomain() })
    }
}
