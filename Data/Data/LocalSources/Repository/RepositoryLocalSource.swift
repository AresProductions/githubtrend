//
//  RepositoryLocalSource.swift
//  Data
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
import CoreData
import Domain
import Foundation

protocol IRepositoryLocalSource {
    func observeTrending() -> AnyPublisher<[Repository], Never>
    func observe(url: String) -> AnyPublisher<Repository?, Never>
    func createOrUpdate(repositories: [Repository]) async -> Result<Void, DomainError>
}

class RepositoryLocalSource: IRepositoryLocalSource {
    private let container: NSPersistentContainer

    init(container: NSPersistentContainer) {
        self.container = container
    }

    func observeTrending() -> AnyPublisher<[Repository], Never> {
        let fetchRequest: NSFetchRequest<DRepository> = DRepository.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(DRepository.name),
                                                         ascending: true,
                                                         selector: #selector(NSString.caseInsensitiveCompare(_:)))]
        return FetchPublisher(request: fetchRequest,
                              context: container.viewContext)
            .map { dRepositories in
                dRepositories.map { dRepository in
                    dRepository.mapToDomain()
                }

            }.eraseToAnyPublisher()
    }

    func observe(url: String) -> AnyPublisher<Repository?, Never> {
        let fetchRequest: NSFetchRequest<DRepository> = DRepository.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "url = %@", url)
        return FetchPublisher(request: fetchRequest,
                              context: container.viewContext)
            .map { dRepositories in
                return dRepositories.first?.mapToDomain()
            }.eraseToAnyPublisher()
    }

    func createOrUpdate(repositories: [Repository]) async -> Result<Void, DomainError> {
        // we're storing trending repositories, thus we need to keep replacing the whole table
        container.deleteModelTable(entityName: "DRepository")
        container.deleteModelTable(entityName: "DContributor")
        return await container.updateAsync { context in
            repositories.forEach { repository in
                let fetchRequest = DRepository.fetchRequest(for: repository.url)

                let dRepository = (try? context.fetch(fetchRequest).first) ?? DRepository(context: context)
                dRepository.updateValues(using: repository)
            }

            try context.saveIfChanged()
        }.mapError { error in
            DomainError.basic(description: error.localizedDescription)
        }
    }
}
