//
//  NSPersistentContainer+Extensions.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Combine
import CoreData
import Foundation

extension NSPersistentContainer {
    // MARK: Creation and Deletion of database

    /// Creates the CoreData container as we need it
    static func createDatabase(testing: Bool = false) -> NSPersistentContainer {
        let model = NSManagedObjectModel.mergedModel(from: [Bundle(identifier: "example.project.Data")!])!
        let container =
            NSPersistentContainer(name: "db" + (testing ? "-testing-\(UUID().uuidString)" : ""),
                                  managedObjectModel: model)

        let description = NSPersistentStoreDescription()
        description.type = NSSQLiteStoreType
        description.url = container.sqliteLocation

        container.persistentStoreDescriptions = [description]
        container.tryLoadStores()

        // get notified when we merge stuff
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.shouldDeleteInaccessibleFaults = true

        return container
    }

    /// The location of the sqlite store for this container
    /// The location is based on the name, thus, in testing, you should use a random name for the container.
    private var sqliteLocation: URL {
        let directory = NSPersistentContainer.defaultDirectoryURL()
        let filename = name + ".sqlite"
        return directory.appendingPathComponent(filename)
    }

    /// Tries to load the peristent store, if it fails it wll recreate the DB
    private func tryLoadStores() {
        loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                print("Failed to load Persistent Store, will recreate DB ", error.localizedDescription)
                self.clearDatabase()
            }
        })
    }

    /// Drops the entire database and recreates it.
    private func clearDatabase() {
        do {
            try persistentStoreCoordinator.destroyPersistentStore(at: sqliteLocation,
                                                                  ofType: NSSQLiteStoreType,
                                                                  options: nil)
        } catch {
            print("Failed to delete CoreData sqlite store: \(String(describing: error))")
        }

        loadStoresOrFail()
    }

    private func loadStoresOrFail() {
        loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible,
                 * due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }

    // MARK: - Modifying data

    @discardableResult func updateAsync<T>(blocking: Bool = false,
                                           task: @escaping (NSManagedObjectContext) throws -> T)
    async -> Result<T, Error> {
        return await withCheckedContinuation { continuation in
            let context = self.updateContext
            let perform = blocking ? context.performAndWait : context.perform

            perform {
                do {
                    continuation.resume(returning: .success(try task(context)))
                } catch {
                    print("Background update task failed: ", String(describing: error))
                    continuation.resume(returning: .failure(error))
                }
            }
        }
    }

    func deleteModelTable(entityName: String) {
        // Specify a batch to delete with a fetch request
        let fetchRequest: NSFetchRequest<NSFetchRequestResult>
        fetchRequest = NSFetchRequest(entityName: entityName)

        // Create a batch delete request for the
        // fetch request
        let deleteRequest = NSBatchDeleteRequest(
            fetchRequest: fetchRequest
        )

        // Specify the result of the NSBatchDeleteRequest
        // should be the NSManagedObject IDs for the
        // deleted objects
        deleteRequest.resultType = .resultTypeObjectIDs

        // Get a reference to a managed object context
        let context = self.viewContext

        // Perform the batch delete
        let batchDelete = try? context.execute(deleteRequest)
            as? NSBatchDeleteResult

        guard let deleteResult = batchDelete?.result
            as? [NSManagedObjectID]
        else { return }

        let deletedObjects: [AnyHashable: Any] = [
            NSDeletedObjectsKey: deleteResult
        ]

        // Merge the delete changes into the managed
        // object context
        NSManagedObjectContext.mergeChanges(
            fromRemoteContextSave: deletedObjects,
            into: [context]
        )
    }

    // we need a memory address
    private enum AssociatedKeys {
        static var updateContextKey: UInt8 = 0
    }

    /// We use **one** dedicated update context to perform updates serially (in the background)
    /// to not provoke merge errors and to ensure that success update calls have the data of the previous one.
    private var updateContext: NSManagedObjectContext {
        var moc = objc_getAssociatedObject(self, &AssociatedKeys.updateContextKey) as? NSManagedObjectContext

        if moc == nil {
            moc = newBackgroundContext()
            moc!.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy

            objc_setAssociatedObject(self, &AssociatedKeys.updateContextKey, moc, .OBJC_ASSOCIATION_RETAIN)
        }

        return moc!
    }
}
