//
//  NSManagedObjectContext+Extensions.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Combine
import CoreData
import Foundation

extension NSManagedObjectContext {
    /// Tries to `save()` the context if it `hasChanges`.
    ///
    /// - Returns: `true` if there were changes saved
    @discardableResult func saveIfChanged() throws -> Bool {
        if hasChanges {
            try save()
            return true
        }

        return false
    }
}
