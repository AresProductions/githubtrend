//
//  SynchronizableManagedObject.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Foundation
import CoreData

/// Protocol for automatically updating arbitary managed objects using other objects
protocol SynchronizableManagedObject: NSManagedObject {
    /// The type of the object we want to use for updating the `NSManagedObject`
    associatedtype OtherObject

    /// Updates the `NSManagedObject` using the `OtherObject`
    func updateValues(using object: OtherObject)
}
