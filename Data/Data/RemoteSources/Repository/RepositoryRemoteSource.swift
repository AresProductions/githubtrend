//
//  RepositoryRemoteSource.swift
//  Data
//
//  Created by Ares Ceka on 28/10/22.
//

import Domain
import Foundation

protocol IRepositoryRemoteSource {
    func getTrending() async -> Result<[Repository], DomainError>
}

class RepositoryRemoteSource: IRepositoryRemoteSource {
    private let restClient: IRestClient

    init(restClient: IRestClient) {
        self.restClient = restClient
    }

    func getTrending() async -> Result<[Repository], DomainError> {
        let result: Result<[RRepository], RestClientError> = await restClient.get(GithubEndpoint.trending)
        let domainResult = result.map { rRepositories in
            rRepositories.map { rRepository in
                rRepository.mapToDomain()
            }
        }.mapError { error in
            error.mapToDomainError()
        }

        return domainResult
    }
}
