//
//  RContributor.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Domain
import Foundation

struct RContributor: Codable {
    let username: String
    let href: String
    let avatar: String
}

extension RContributor {
    func mapToDomain() -> Contributor {
        Contributor(username: self.username,
                    profileURL: self.href,
                    avatarImageURL: self.avatar)
    }
}
