//
//  RRepository.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Domain
import Foundation

// MARK: - RepositoryElement

struct RRepository: Codable {
    let author: String
    let name: String
    let avatar: String
    let url: String
    let description: String
    let language: String?
    let languageColor: String?
    let stars, forks, currentPeriodStars: Int
    let builtBy: [RContributor]
}

extension RRepository {
    func mapToDomain() -> Repository {
        Repository(name: self.name,
                   author: self.author,
                   avatarImageURL: self.avatar,
                   url: self.url,
                   description: self.description,
                   language: self.language,
                   languageColorHexCode: self.languageColor,
                   stars: self.stars,
                   forks: self.forks,
                   contributors: self.builtBy.map { $0.mapToDomain() })
    }
}
