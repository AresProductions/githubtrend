//
//  RestClient.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Foundation

protocol IRestClient {
    /// Retrieves a JSON resource and decodes it
    func get<T: Decodable, E: Endpoint>(_ endpoint: E) async -> Result<T, RestClientError>
}

class RestClient: IRestClient {
    private let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    func get<T, E>(_ endpoint: E) async -> Result<T, RestClientError> where T: Decodable, E: Endpoint {
        var request = URLRequest(url: endpoint.url)
        request.httpMethod = "GET"

        do {
            let (data, urlResponse) = try await session.data(for: request)
            // swiftlint:disable:next force_cast
            let response = urlResponse as! HTTPURLResponse
            let code = response.statusCode

            if code == 200 {
                return parseData(data: data)
            } else {
                return .failure(RestClientError.invalidResponse(code: code))
            }
        } catch {
            return .failure(RestClientError.invalidRequest(message: error.localizedDescription))
        }
    }

    private func parseData<T: Decodable>(data: Data) -> Result<T, RestClientError> {
        do {
            let parseJson = try JSONDecoder().decode(T.self, from: data)
            return .success(parseJson)
        } catch {
            return .failure(RestClientError.failedDecoding(message: error.localizedDescription))
        }
    }
}
