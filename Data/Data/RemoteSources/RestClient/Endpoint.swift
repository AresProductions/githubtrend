//
//  Endpoint.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Foundation

protocol Endpoint {
    var url: URL { get }
    var path: String { get }
}

/// Football API Endpoints
enum GithubEndpoint: Endpoint {
    case trending

    var path: String {
        switch self {
        case .trending:
            return "" // API doesn't contain any real path
        }
    }

    var url: URL {
        return URL(string: "\(GithubEndpoint.baseURL)\(path)")!
    }

    static let baseURL = URL(string: "https://github-trending-api-wonder.herokuapp.com/")!
}
