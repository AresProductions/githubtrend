//
//  RestClientError.swift
//  Data
//
//  Created by Ares Ceka on 29/10/22.
//

import Domain
import Foundation

enum RestClientError: Error {
    case invalidRequest(message: String)
    case invalidResponse(code: Int)
    case failedDecoding(message: String)
}

extension RestClientError {
    func mapToDomainError() -> DomainError {
        switch self {
        case .invalidRequest(let message):
            return DomainError.remote(description: "Invalid request: \(message)")
        case .invalidResponse(let code):
            return DomainError.remote(description: "Invalid response: \(code)")
        case .failedDecoding(let message):
            return DomainError.remote(description: "Invalid decoding: \(message)")
        }
    }
}
