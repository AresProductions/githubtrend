//
//  DataComponent.swift
//  Data
//
//  Created by Ares Ceka on 30/10/22.
//

import CoreData
import Domain
import Foundation

public protocol DataComponent {
    func repositoryRepo() -> IRepositoryRepo
}

public class DataModule: DataComponent {
    let restClient: IRestClient = RestClient(session: URLSession.shared)
    let container = NSPersistentContainer.createDatabase(testing: false)

    public init() {}

    public func repositoryRepo() -> IRepositoryRepo {
        RepositoryRepo(localSource: RepositoryLocalSource(container: container),
                       remoteSource: RepositoryRemoteSource(restClient: restClient))
    }
}
