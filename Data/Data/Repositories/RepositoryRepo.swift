//
//  RepositoryRepo.swift
//  Data
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
import Domain
import Foundation

class RepositoryRepo: IRepositoryRepo {
    private let localSource: IRepositoryLocalSource
    private let remoteSource: IRepositoryRemoteSource

    init(localSource: IRepositoryLocalSource, remoteSource: IRepositoryRemoteSource) {
        self.localSource = localSource
        self.remoteSource = remoteSource
    }

    func syncTrending() async -> Result<Void, DomainError> {
        do {
            let newRepositories = try await remoteSource.getTrending().get()
            _ = try await localSource.createOrUpdate(repositories: newRepositories).get()
            return .success(())
        } catch {
            let domainError = error as? DomainError
            return .failure(domainError ?? DomainError.unknown(description: error.localizedDescription))
        }
    }

    func observeTrending() -> AnyPublisher<[Repository], Never> {
        localSource.observeTrending()
    }

    func observe(url: String) -> AnyPublisher<Repository?, Never> {
        localSource.observe(url: url)
    }
}
