//
//  TrendingListViewScreenshotTests.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Domain
import SnapshotTesting
import SwiftUI
@testable import GithubTrend
import XCTest

/***
 PLEASE USE SIMULATOR IPHONE 14 PRO, otherwise the snapshot tests will fail!
 */
class TrendingListViewScreenshotTests: XCTestCase {
    func testListStateIphone13Pro() {
        let repository = Repository(
            name: "repository name",
            author: "author",
            avatarImageURL: "",
            url: "https://github.com/somelink/repository",
            description: "Some description which needs to be shown with long text",
            language: "Go",
            languageColorHexCode: "#00ADD8",
            stars: 833,
            forks: 35,
            contributors: [
                Contributor(username: "contributor name a",
                            profileURL: "https://github.com/profileA",
                            avatarImageURL: ""),
                Contributor(username: "contributor name b",
                            profileURL: "https://github.com/profileB",
                            avatarImageURL: ""),
            ])

        let repository2 = Repository(
            name: "repository name 2",
            author: "author 2",
            avatarImageURL: "",
            url: "https://github.com/somelink/repository2",
            description: "Some short description",
            language: nil,
            languageColorHexCode: nil,
            stars: 10,
            forks: 35,
            contributors: [
                Contributor(username: "contributor name c",
                            profileURL: "https://github.com/profileA",
                            avatarImageURL: ""),
            ])

        let mockSyncTrendingRepositoriesUC = MockSyncTrendingRepositoriesUC()
        let mockObserveTrendingRepositoriesUC = MockObserveTrendingRepositoriesUC(
            expectedRepositories: [repository, repository2])
        let contentView = TrendingListView(
            viewModel: TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                             observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC))
        let view: UIViewController = UIHostingController(rootView: contentView)

        assertSnapshot(matching: view, as: .wait(for: 0.1, on: .image(on: .iPhone13Pro,
                                                                      traits: ScreenshotTraits.lightMode)))
    }

    func testEmptyListStateIphone13Pro() {
        let mockSyncTrendingRepositoriesUC = MockSyncTrendingRepositoriesUC()
        let mockObserveTrendingRepositoriesUC = MockObserveTrendingRepositoriesUC(expectedRepositories: [])
        let contentView = TrendingListView(
            viewModel: TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                             observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC))
        let view: UIViewController = UIHostingController(rootView: contentView)

        assertSnapshot(matching: view, as: .wait(for: 0.1, on: .image(on: .iPhone13Pro,
                                                                      traits: ScreenshotTraits.lightMode)))
    }
}
