//
//  RepositoryDetailsViewScreenshotTests.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Domain
import SnapshotTesting
import SwiftUI
@testable import GithubTrend
import XCTest

/***
 PLEASE USE SIMULATOR IPHONE 14 PRO, otherwise the snapshot tests will fail!
 */
class RepositoryDetailsViewScreenshotTests: XCTestCase {
    func testDetailsStateIphone13Pro() throws {
        let repository = Repository(
            name: "repository name",
            author: "author",
            avatarImageURL: "",
            url: "https://github.com/somelink/repository",
            description: "Some description which needs to be shown with long text",
            language: "Go",
            languageColorHexCode: "#00ADD8",
            stars: 833,
            forks: 35,
            contributors: [
                Contributor(username: "contributor name a",
                            profileURL: "https://github.com/profileA",
                            avatarImageURL: ""),
                Contributor(username: "contributor name b",
                            profileURL: "https://github.com/profileB",
                            avatarImageURL: ""),
            ])

        let observeRepositoryUC = MockObserveRepositoryUC(expectedRepository: repository)
        let contentView = RepositoryDetailsView(repositoryURL: "url", viewModel:
            RepositoryDetailsViewModel(repositoryURL: "url", observeRepositoryUC: observeRepositoryUC))
        let view: UIViewController = UIHostingController(rootView: contentView)

        assertSnapshot(matching: view, as: .wait(for: 0.5, on: .image(on: .iPhone13Pro,
                                                                      traits: ScreenshotTraits.lightMode)))
    }

    func testNotFoundStateIphone13Pro() throws {
        let observeRepositoryUC = MockObserveRepositoryUC(expectedRepository: nil)
        let contentView = RepositoryDetailsView(repositoryURL: "url", viewModel:
            RepositoryDetailsViewModel(repositoryURL: "url", observeRepositoryUC: observeRepositoryUC))
        let view: UIViewController = UIHostingController(rootView: contentView)

        assertSnapshot(matching: view, as: .image(on: .iPhone13Pro,
                                                  traits: ScreenshotTraits.lightMode))
    }
}
