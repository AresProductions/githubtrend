//
//  ScreenshotTraits.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 1/11/22.
//

import Foundation
import SwiftUI

enum ScreenshotTraits {
    static let lightMode = UITraitCollection(userInterfaceStyle: .light)
    static let darkMode = UITraitCollection(userInterfaceStyle: .dark)
}
