//
//  MockObserveRepositoryUC.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import Combine
import Domain

class MockObserveRepositoryUC: IObserveRepositoryUC {
    let expectedRepository: Repository?

    var executeSpy: [String] = []

    init(expectedRepository: Repository? = nil) {
        self.expectedRepository = expectedRepository
    }

    func execute(url: String) -> AnyPublisher<Repository?, Never> {
        executeSpy.append(url)
        return Just(expectedRepository).eraseToAnyPublisher()
    }
}
