//
//  MockObserveTrendingRepositoriesUC.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import Combine
import Domain

class MockObserveTrendingRepositoriesUC: IObserveTrendingRepositoriesUC {
    let expectedRepositories: [Repository]

    var executeSpy: [Void] = []

    init(expectedRepositories: [Repository] = []) {
        self.expectedRepositories = expectedRepositories
    }

    func execute() -> AnyPublisher<[Repository], Never> {
        executeSpy.append(())
        return Just(expectedRepositories).eraseToAnyPublisher()
    }
}
