//
//  MockSyncTrendingRepositoriesUC.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import Combine
import Domain

class MockSyncTrendingRepositoriesUC: ISyncTrendingRepositoriesUC {
    let expectedResult: Result<Void, DomainError>

    var executeSpy: [Void] = []

    init(expectedResult: Result<Void, DomainError> = .success(())) {
        self.expectedResult = expectedResult
    }

    func execute() async -> Result<Void, DomainError> {
        executeSpy.append(())
        return expectedResult
    }
}
