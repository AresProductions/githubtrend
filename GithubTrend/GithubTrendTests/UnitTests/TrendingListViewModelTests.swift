//
//  TrendingListViewModelTests.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Domain
import Foundation
import Nimble
@testable import GithubTrend
import XCTest

class TrendingListViewModelTests: XCTestCase {
    func testListViewState() throws {
        let repositories = [anyRepository(), anyRepository(), anyRepository()]

        let mockSyncTrendingRepositoriesUC = MockSyncTrendingRepositoriesUC()
        let mockObserveTrendingRepositoriesUC =
            MockObserveTrendingRepositoriesUC(expectedRepositories: repositories)
        let viewModel = TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                              observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC)

        // then
        let expectedViewState =
            TrendingListViewState.list(repositories.map { $0.mapToTrendingItemViewState() })
        expect(viewModel.viewState).toEventually(equal(expectedViewState), timeout: .milliseconds(500))
    }

    func testEmptyListViewState() {
        let mockSyncTrendingRepositoriesUC = MockSyncTrendingRepositoriesUC()
        let mockObserveTrendingRepositoriesUC =
            MockObserveTrendingRepositoriesUC(expectedRepositories: [])
        let viewModel = TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                              observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC)

        // then
        let expectedViewState = TrendingListViewState.emptyList
        expect(viewModel.viewState).toEventually(equal(expectedViewState))
    }

    func testSyncActionGivenSyncUCFails() {
        // given
        let mockSyncTrendingRepositoriesUC =
            MockSyncTrendingRepositoriesUC(expectedResult: .failure(DomainError.basic(description: "sync error")))
        let mockObserveTrendingRepositoriesUC =
            MockObserveTrendingRepositoriesUC(expectedRepositories: [])

        // when
        let viewModel = TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                              observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC)
        expect(mockSyncTrendingRepositoriesUC.executeSpy.count).toEventually(equal(1)) // when inited, sync runs
        expect(viewModel.syncErrorAlert).toEventually(beTrue()) // because sync failed

        viewModel.syncErrorAlert = false // reset flag

        // when
        viewModel.action(.refresh)

        // then
        expect(viewModel.syncErrorAlert).toEventually(beTrue())
        expect(mockSyncTrendingRepositoriesUC.executeSpy.count).to(equal(2)) // because sync action was called
    }

    func testSyncActionGivenSyncUCSucceeds() {
        // given
        let mockSyncTrendingRepositoriesUC = MockSyncTrendingRepositoriesUC()
        let mockObserveTrendingRepositoriesUC =
            MockObserveTrendingRepositoriesUC(expectedRepositories: [])

        // when
        let viewModel = TrendingListViewModel(syncTrendingRepositoriesUC: mockSyncTrendingRepositoriesUC,
                                              observeTrendingRepositoriesUC: mockObserveTrendingRepositoriesUC)
        expect(mockSyncTrendingRepositoriesUC.executeSpy.count).toEventually(equal(1)) // when inited, sync runs
        expect(viewModel.syncErrorAlert).to(beFalse())

        // when
        viewModel.action(.refresh)

        // then
        expect(viewModel.syncErrorAlert).toEventually(beFalse()) // will remain false
        expect(mockSyncTrendingRepositoriesUC.executeSpy.count)
            .toEventually(equal(2)) // because sync action was called
    }
}
