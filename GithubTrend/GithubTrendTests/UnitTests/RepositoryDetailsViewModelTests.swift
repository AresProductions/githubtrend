//
//  RepositoryDetailsViewModelTests.swift
//  GithubTrendTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Domain
import Foundation
import Nimble
@testable import GithubTrend
import XCTest

class RepositoryDetailsViewModelTests: XCTestCase {
    func testDetailsViewState() throws {
        // given
        let repository = anyRepository()
        let observeRepositoryUC = MockObserveRepositoryUC(expectedRepository: repository)

        // when
        let viewModel = RepositoryDetailsViewModel(repositoryURL: "url", observeRepositoryUC: observeRepositoryUC)

        // then
        let expectedViewState =
            RepositoryDetailsViewState.details(details: repository.mapToRepositoryDetailsViewState())
        expect(viewModel.viewState).toEventually(equal(expectedViewState), timeout: .milliseconds(500))
    }

    func testNotFoundViewState() throws {
        // given
        let observeRepositoryUC = MockObserveRepositoryUC(expectedRepository: nil)

        // when
        let viewModel = RepositoryDetailsViewModel(repositoryURL: "url", observeRepositoryUC: observeRepositoryUC)

        // then
        let expectedViewState = RepositoryDetailsViewState.notFound
        expect(viewModel.viewState).to(equal(expectedViewState))
    }
}
