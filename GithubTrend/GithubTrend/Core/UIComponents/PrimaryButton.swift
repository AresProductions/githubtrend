//
//  PrimaryButton.swift
//  GithubTrend
//
//  Created by Ares Ceka on 1/11/22.
//

import SwiftUI

/// Primary Button
struct PrimaryButton<Label>: View where Label: View {
    @Environment(\.isEnabled) private var isEnabled: Bool

    private let action: () -> Void
    private let label: () -> Label
    private let height: CGFloat
    private let width: CGFloat?
    public init(height: CGFloat = CGFloat.theme.buttonHeight,
                width: CGFloat? = nil,
                action: @escaping () -> Void,
                @ViewBuilder label: @escaping () -> Label)
    {
        self.action = action
        self.label = label
        self.height = height
        self.width = width
    }

    var body: some View {
        Button(action: action, label: label).buttonStyle(PrimaryButtonStyle(!isEnabled, height, width))
    }
}

private struct PrimaryButtonStyle: ButtonStyle {
    private let disabled: Bool
    private let height: CGFloat
    private let width: CGFloat?
    init(_ disabled: Bool = false, _ height: CGFloat, _ width: CGFloat?) {
        self.disabled = disabled
        self.height = height
        self.width = width
    }

    private func backgroundColor(isPressed: Bool, disabled: Bool) -> some View {
        if isPressed {
            return Color.theme.primaryPressed
        } else if disabled {
            return Color.theme.primary.opacity(0.3)
        } else {
            return Color.theme.primary
        }
    }

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: height,
                   maxWidth: width,
                   minHeight: height,
                   idealHeight: height,
                   maxHeight: height,
                   alignment: .center)
            .padding([.leading, .trailing], CGFloat.theme.padding2)
            .font(Font.theme.button)
            .foregroundColor(Color.theme.onPrimary)
            .background(backgroundColor(isPressed: configuration.isPressed, disabled: disabled))
            .cornerRadius(CGFloat.theme.padding4)
    }
}

#if canImport(SwiftUI) && DEBUG

    struct PrimaryButton_Previews: PreviewProvider {
        static var previews: some View {
            AnyView(VStack {
                PrimaryButton(action: {}, label: {
                    Text("Enabled Button")
                }).padding(.vertical, 10)

                PrimaryButton(action: {}, label: {
                    Text("Disabled Button")
                }).disabled(true).padding(.vertical, 10)
            })
        }
    }

#endif
