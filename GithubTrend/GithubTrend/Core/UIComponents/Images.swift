//
//  Images.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation
import SwiftUI

enum Images {
    static let emptyBox = Image("emptyList")
    static let star = Image("star")
    static let placeholder = Image("placeholder")
    static let xapoLogo = Image("xapo_logo")
}
