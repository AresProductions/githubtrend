//
//  View+Extensions.swift
//  GithubTrend
//
//  Created by Ares Ceka on 1/11/22.
//

import Foundation
import SwiftUI

extension View {
    func hideNavigationBar() -> some View {
        modifier(HideNavigationBarViewModifier())
    }
}

private struct HideNavigationBarViewModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
    }
}
