//
//  ViewState.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation

protocol ViewState {}
