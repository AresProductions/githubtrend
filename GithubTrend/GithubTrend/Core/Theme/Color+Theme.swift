//
//  Color+Theme.swift
//  GithubTrend
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import SwiftUI

protocol IThemeColors {
    /// Primary UI color
    static var primary: Color { get }
    /// Primary UI color when is pressed
    static var primaryPressed: Color { get }
    /// On Primary UI color
    static var onPrimary: Color { get }
    /// Background UI color
    static var background: Color { get }
    /// On Background UI color
    static var onBackground: Color { get }
}

private struct ThemeColors: IThemeColors {
    static var primary = Color("primary")
    static var primaryPressed = Color("primary_pressed")
    static var onPrimary = Color("on_primary")
    static var background = Color("background")
    static var onBackground = Color("on_background")
}

extension Color {
    /// Theme Colors
    static var theme: IThemeColors.Type {
        return ThemeColors.self
    }
}
