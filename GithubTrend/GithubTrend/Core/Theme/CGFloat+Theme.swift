//
//  CGFloat+Extensions.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import SwiftUI

protocol IThemeSizes {
    /// Quarter padding Size (2)
    static var quarterPadding: CGFloat { get }

    /// Half padding Size (4)
    static var halfPadding: CGFloat { get }

    /// Padding Size (8)
    static var padding: CGFloat { get }

    /// Padding Size (12)
    static var padding1_5: CGFloat { get }

    /// Padding Size (16)
    static var padding2: CGFloat { get }

    /// Padding Size (20)
    static var padding2_5: CGFloat { get }

    /// Padding Size 24)
    static var padding3: CGFloat { get }

    /// Padding Size (32)
    static var padding4: CGFloat { get }

    /// Padding Size (36)
    static var padding4_5: CGFloat { get }

    /// Padding Size (40)
    static var padding5: CGFloat { get }

    /// Padding Size (48)
    static var padding6: CGFloat { get }

    /// Padding Size (56)
    static var padding7: CGFloat { get }

    /// Padding Size (64)
    static var padding8: CGFloat { get }

    /// Padding Size (256)
    static var padding32: CGFloat { get }

    /// Height and Width for icons
    static var icon: CGFloat { get }

    /// Height and Width for big icons
    static var bigIcon: CGFloat { get }

    /// Button Height
    static var buttonHeight: CGFloat { get }

    /// Logo Height and Width
    static var logo: CGFloat { get }
}

private struct ThemeSizes: IThemeSizes {
    static var quarterPadding: CGFloat = 2

    static var halfPadding: CGFloat = 4

    static var padding: CGFloat = 8

    static var padding1_5: CGFloat = padding * 1.5

    static var padding2: CGFloat = padding * 2

    static var padding2_5: CGFloat = padding * 2.5

    static var padding3: CGFloat = padding * 3

    static var padding4: CGFloat = padding * 4

    static var padding4_5: CGFloat = padding * 4.5

    static var padding5: CGFloat = padding * 5

    static var padding6: CGFloat = padding * 6

    static var padding7: CGFloat = padding * 7

    static var padding8: CGFloat = padding * 8

    static var padding32: CGFloat = padding * 32

    static var icon: CGFloat = padding * 2

    static var bigIcon: CGFloat = padding * 8

    static var buttonHeight: CGFloat = padding * 6

    static var logo: CGFloat = padding * 10
}

extension CGFloat {
    /// Theme Dimensions
    static var theme: IThemeSizes.Type {
        return ThemeSizes.self
    }
}
