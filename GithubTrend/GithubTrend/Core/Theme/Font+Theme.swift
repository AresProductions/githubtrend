//
//  Font+Theme.swift
//  GithubTrend
//
//  Created by Ares Ceka on 1/11/22.
//

import Foundation
import SwiftUI

/// Theme Fonts
protocol IThemeFonts {
    /// Title Font
    static var title: Font { get }

    /// Subtitle Font
    static var subtitle: Font { get }

    /// Body Font
    static var body: Font { get }

    /// Button Font
    static var button: Font { get }

    /// Caption Font
    static var caption: Font { get }
}

private struct ThemeFonts: IThemeFonts {
    static var title = Font.system(size: 36, weight: .bold, design: .default)
    static var subtitle = Font.system(size: 16, weight: .bold, design: .default)
    static var body = Font.system(size: 16, weight: .regular, design: .default)
    static var caption = Font.system(size: 14, weight: .regular, design: .default)
    static var button = Font.system(size: 17, weight: .medium, design: .default)
}

extension Font {
    /// Theme Fonts
    static var theme: IThemeFonts.Type {
        return ThemeFonts.self
    }
}

#if canImport(SwiftUI) && DEBUG

struct FontDescr: Identifiable {
    var id = UUID()
    var font: Font
    var description: String
}

struct ThemeFonts_Previews: PreviewProvider {
    private static func fonts() -> [FontDescr] {
        return [(Font.theme.title, "Title"),
                (Font.theme.subtitle, "Subtitle"),
                (Font.theme.body, "Body"),
                (Font.theme.button, "Button"),
                (Font.theme.caption, "Caption")]
            .map { FontDescr(font: $0.0, description: $0.1) }
    }

    static var previews: some View {
        VStack {
            ForEach(fonts()) { fontDescr in
                Text(fontDescr.description)
                    .font(fontDescr.font)
                    .padding(10)
            }
        }.previewLayout(.sizeThatFits)
            .padding(20)
    }
}

#endif
