//
//  LanguageStarsView.swift
//  GithubTrend
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import SwiftUI

struct LanguageStarsView: View {
    let viewState: LanguageStarsViewState

    var body: some View {
        HStack(alignment: .center) {
            if let languageColor = viewState.languageColor, let language = viewState.language {
                Circle()
                    .fill(languageColor)
                    .frame(width: CGFloat.theme.icon, height: CGFloat.theme.icon)
                Text(language)
                    .font(Font.theme.body)
            }

            Spacer()
            Images.star
                .resizable()
                .frame(width: CGFloat.theme.icon, height: CGFloat.theme.icon)
            Text(String(viewState.stars))
                .font(Font.theme.body)
        }
    }
}
