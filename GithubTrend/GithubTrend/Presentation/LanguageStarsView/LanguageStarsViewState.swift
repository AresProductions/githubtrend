//
//  LanguageStarsViewState.swift
//  GithubTrend
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import SwiftUI

struct LanguageStarsViewState {
    let languageColor: Color?
    let language: String?
    let stars: Int
}
