//
//  TrendingListViewState.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Domain
import Foundation
import SwiftUI

enum TrendingListViewState: ViewState, Equatable {
    case list(_ list: [TrendingItemViewState])
    case emptyList
}
