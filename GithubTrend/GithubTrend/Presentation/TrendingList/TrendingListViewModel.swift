//
//  TrendingListViewModel.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Combine
import Domain
import Foundation
import SwiftUI

class TrendingListViewModel: ObservableObject {
    private let syncTrendingRepositoriesUC: ISyncTrendingRepositoriesUC
    private let observeTrendingRepositoriesUC: IObserveTrendingRepositoriesUC

    private var cancellables: [AnyCancellable] = []
    init(syncTrendingRepositoriesUC: ISyncTrendingRepositoriesUC,
         observeTrendingRepositoriesUC: IObserveTrendingRepositoriesUC)
    {
        self.syncTrendingRepositoriesUC = syncTrendingRepositoriesUC
        self.observeTrendingRepositoriesUC = observeTrendingRepositoriesUC
        updateTrendingList()
        syncTrendingList()
    }

    @Published var viewState = TrendingListViewState.emptyList
    @Published var syncErrorAlert = false

    func action(_ action: TrendingListAction) {
        switch action {
        case .refresh: syncTrendingList()
        }
    }

    private func updateTrendingList() {
        // Use Concatenate with Debounce to avoid flickerings
        let repositoriesPublisher = Publishers.Concatenate(
            prefix: observeTrendingRepositoriesUC.execute().first(),
            suffix: observeTrendingRepositoriesUC.execute().debounce(for: .milliseconds(300),
                                                                     scheduler: DispatchQueue.main)
        )

        repositoriesPublisher.sink { [weak self] repositories in
            withAnimation {
                if repositories.isEmpty {
                    self?.viewState = .emptyList
                } else {
                    self?.viewState = .list(repositories.map { $0.mapToTrendingItemViewState() })
                }
            }
        }.store(in: &cancellables)
    }

    private func syncTrendingList() {
        Task {
            let syncResult = await syncTrendingRepositoriesUC.execute()
            await updateErrorAlert(syncResult: syncResult)
        }
    }

    @MainActor
    private func updateErrorAlert(syncResult: Result<Void, DomainError>) async {
        switch syncResult {
        case .success:
            print("Downloaded new repos")
        case .failure:
            syncErrorAlert = true
        }
    }
}
