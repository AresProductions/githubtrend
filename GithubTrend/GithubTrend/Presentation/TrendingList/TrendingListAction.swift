//
//  TrendingListAction.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation

enum TrendingListAction: Action {
    case refresh
}
