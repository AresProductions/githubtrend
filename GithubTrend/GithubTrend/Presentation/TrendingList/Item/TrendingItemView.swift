//
//  TrendingItemView.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation
import SwiftUI

struct TrendingItemView: View {
    let viewState: TrendingItemViewState

    var body: some View {
        VStack(alignment: .leading) {
            Text(viewState.name)
                .font(Font.theme.body)
                .bold()
                .padding(.bottom, CGFloat.theme.quarterPadding)
            Text(viewState.description).lineLimit(3)
                .font(Font.theme.body)
            LanguageStarsView(viewState: LanguageStarsViewState(
                languageColor: viewState.languageColor,
                language: viewState.language,
                stars: viewState.stars))
        }
    }
}
