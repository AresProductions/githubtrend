//
//  TrendingItemViewState.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Domain
import Foundation
import SwiftUI

struct TrendingItemViewState: ViewState, Identifiable, Hashable, Equatable {
    var id: String {
        url
    }

    var url: String
    var name: String
    var description: String
    var language: String?
    var languageColor: Color?
    var stars: Int
}

extension Repository {
    func mapToTrendingItemViewState() -> TrendingItemViewState {
        TrendingItemViewState(url: self.url,
                              name: self.name,
                              description: self.description,
                              language: self.language,
                              languageColor: Color(hex: languageColorHexCode),
                              stars: self.stars)
    }
}
