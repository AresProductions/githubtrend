//
//  TrendingListView.swift
//  GithubTrend
//
//  Created by Ares Ceka on 28/10/22.
//

import SwiftUI

struct TrendingListView: View {
    @StateObject var viewModel: TrendingListViewModel

    init(viewModel: TrendingListViewModel? = nil) {
        if let viewModel = viewModel {
            self._viewModel = StateObject(wrappedValue: viewModel)
        } else {
            self._viewModel = StateObject(wrappedValue: applicationComponent.trendingListViewModel())
        }
    }

    var body: some View {
        ZStack {
            switch viewModel.viewState {
            case .emptyList: emptyListView()
            case .list(let list): repoListView(list)
            }
        }.alert(isPresented: $viewModel.syncErrorAlert, content: {
            Alert(title: Text("Error"), message: Text("Failed to download new trending Repositories"))
        })
        .navigationTitle(Text("Trending Repositories"))
        .navigationBarTitleDisplayMode(.large)
        .navigationBarHidden(false)
    }

    private func repoListView(_ list: [TrendingItemViewState]) -> some View {
        List(list) { item in
            NavigationLink {
                RepositoryDetailsView(repositoryURL: item.url)
            } label: {
                TrendingItemView(viewState: item)
            }
        }.refreshable {
            viewModel.action(.refresh)
        }
        .listStyle(PlainListStyle())
    }

    private func emptyListView() -> some View {
        VStack {
            Images.emptyBox
                .resizable()
                .tint(Color.theme.onBackground)
                .frame(width: CGFloat.theme.bigIcon,
                       height: CGFloat.theme.bigIcon)
            Text("No repositories to show. Please refresh")
                .font(Font.theme.body)
                .padding(.bottom, CGFloat.theme.halfPadding)
            Button("Refresh") {
                viewModel.action(.refresh)
            }
        }
    }
}

#if canImport(SwiftUI) && DEBUG

struct TrendingListView_Previews: PreviewProvider {
    static var previews: some View {
        TrendingListView()
    }
}

#endif
