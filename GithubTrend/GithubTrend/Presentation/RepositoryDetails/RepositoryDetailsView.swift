//
//  RepositoryDetailsView.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation
import Kingfisher
import SwiftUI

struct RepositoryDetailsView: View {
    let repositoryURL: String
    @StateObject var viewModel: RepositoryDetailsViewModel

    init(repositoryURL: String, viewModel: RepositoryDetailsViewModel? = nil) {
        self.repositoryURL = repositoryURL
        if let viewModel = viewModel {
            self._viewModel = StateObject(wrappedValue: viewModel)
        } else {
            self._viewModel = StateObject(wrappedValue: applicationComponent.repositoryDetailsViewModel(
                repositoryURL: repositoryURL))
        }
    }

    var body: some View {
        Group {
            switch viewModel.viewState {
            case .details(let details): loadDetailsView(details: details)
            case .notFound: loadErrorView()
            }
        }.padding()
            .navigationBarTitleDisplayMode(.inline)
    }

    private func loadErrorView() -> some View {
        VStack {
            Spacer()
            Text("Failed to load repository")
                .font(Font.theme.title)
            Spacer()
        }
    }

    private func loadDetailsView(details: RepositoryDetailsViewState.Details) -> some View {
        ScrollView {
            VStack(alignment: .leading) {
                avatarAndNameView(details)
                urlView(details.url)
                Text(details.description)
                    .font(Font.theme.body)

                LanguageStarsView(
                    viewState: LanguageStarsViewState(
                        languageColor: details.languageColor,
                        language: details.language,
                        stars: details.stars))
                    .padding(.bottom)

                contributorsView(details.contributors)
                Spacer()
            }
        }
    }

    private func avatarAndNameView(_ details: RepositoryDetailsViewState.Details) -> some View {
        HStack(alignment: .center) {
            KFImage(details.avatarImageURL)
                .fade(duration: 0.2)
                .placeholder {
                    Images.placeholder
                        .resizable()
                        .tint(Color.theme.onBackground)
                        .frame(width: CGFloat.theme.bigIcon,
                               height: CGFloat.theme.bigIcon)
                }
                .resizable()
                .frame(width: CGFloat.theme.bigIcon, height: CGFloat.theme.bigIcon)
            Text(details.name)
                .font(Font.theme.title)
                .padding(.bottom, CGFloat.theme.quarterPadding)
        }
    }

    private func urlView(_ url: String) -> some View {
        Text(url)
            .font(Font.theme.body)
            .bold()
            .padding(.bottom, CGFloat.theme.quarterPadding)
    }

    private func contributorsView(_ contributors: [RepositoryDetailsViewState.ContributorDetails]) -> some View {
        VStack(alignment: .leading) {
            Text("Contributors")
                .font(Font.theme.subtitle)
            ForEach(contributors) { contributor in
                HStack {
                    KFImage(contributor.avatarImageUrl)
                        .fade(duration: 0.2)
                        .placeholder {
                            Images.placeholder
                                .resizable()
                                .frame(width: CGFloat.theme.icon,
                                       height: CGFloat.theme.icon)
                        }
                        .resizable()
                        .frame(width: CGFloat.theme.icon, height: CGFloat.theme.icon)
                    Text(contributor.username)
                        .font(Font.theme.body)
                }
            }.padding(.leading, CGFloat.theme.halfPadding)
        }
    }
}

#if canImport(SwiftUI) && DEBUG

struct RepositoryDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        RepositoryDetailsView(repositoryURL: "")
    }
}

#endif
