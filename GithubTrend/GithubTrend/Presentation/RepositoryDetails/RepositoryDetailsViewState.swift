//
//  RepositoryDetailsViewState.swift
//  GithubTrend
//
//  Created by Ares Ceka on 31/10/22.
//

import Domain
import Foundation
import SwiftUI

enum RepositoryDetailsViewState: ViewState, Equatable {
    case details(details: Details)
    case notFound

    struct Details: Equatable {
        var url: String
        var name: String
        var avatarImageURL: URL?
        var description: String
        var language: String?
        var languageColor: Color?
        var stars: Int
        var contributors: [ContributorDetails]
    }

    struct ContributorDetails: Identifiable, Equatable {
        var id: String {
            username
        }

        var avatarImageUrl: URL?
        var username: String
    }
}

extension Repository {
    func mapToRepositoryDetailsViewState() -> RepositoryDetailsViewState.Details {
        RepositoryDetailsViewState.Details(
            url: self.url,
            name: self.name,
            avatarImageURL: URL(string: self.avatarImageURL),
            description: self.description,
            language: self.language,
            languageColor: Color(hex: languageColorHexCode),
            stars: self.stars,
            contributors: contributors.map {
                RepositoryDetailsViewState.ContributorDetails(
                    avatarImageUrl: URL(string: $0.avatarImageURL),
                    username: $0.username)
            })
    }
}
