//
//  RepositoryDetailsViewModel.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Combine
import Domain
import Foundation

class RepositoryDetailsViewModel: ObservableObject {
    private let repositoryURL: String
    private let observeRepositoryUC: IObserveRepositoryUC
    private var cancellables: [AnyCancellable] = []
    init(repositoryURL: String, observeRepositoryUC: IObserveRepositoryUC) {
        self.repositoryURL = repositoryURL
        self.observeRepositoryUC = observeRepositoryUC
        observeRepository()
    }

    @Published var viewState = RepositoryDetailsViewState.notFound

    private func observeRepository() {
        observeRepositoryUC.execute(url: repositoryURL)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] repository in
                if let repository = repository {
                    self?.viewState = .details(details: repository.mapToRepositoryDetailsViewState())
                } else {
                    print("Failed to load repository")
                    self?.viewState = .notFound
                }
            }).store(in: &cancellables)
    }
}
