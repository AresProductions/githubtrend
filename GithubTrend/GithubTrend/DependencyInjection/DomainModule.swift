//
//  DomainComponent.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation
import Domain
import Data

class DomainModule: DomainComponent {
    let dataComponent: DataComponent
    init(dataComponent: DataComponent) {
        self.dataComponent = dataComponent
    }

    func observeTrendingRepositoriesUC() -> IObserveTrendingRepositoriesUC {
        ObserveTrendingRepositoriesUC(repo: dataComponent.repositoryRepo())
    }

    func syncTrendingRepositoriesUC() -> ISyncTrendingRepositoriesUC {
        SyncTrendingRepositoriesUC(repo: dataComponent.repositoryRepo())
    }

    func observeRepositoryUC() -> IObserveRepositoryUC {
        ObserveRepositoryUC(repo: dataComponent.repositoryRepo())
    }
}
