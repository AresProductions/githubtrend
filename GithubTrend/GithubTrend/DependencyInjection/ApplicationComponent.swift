//
//  ApplicationComponent.swift
//  GithubTrend
//
//  Created by Ares Ceka on 30/10/22.
//

import Data
import Domain
import Foundation

protocol ApplicationComponent {
    func trendingListViewModel() -> TrendingListViewModel
    func repositoryDetailsViewModel(repositoryURL: String) -> RepositoryDetailsViewModel
}

class ApplicationModule: ApplicationComponent {
    private let dataComponent: DataComponent
    private let domainComponent: DomainComponent

    init(dataComponent: DataComponent, domainComponent: DomainComponent) {
        self.dataComponent = dataComponent
        self.domainComponent = domainComponent
    }

    func trendingListViewModel() -> TrendingListViewModel {
        TrendingListViewModel(syncTrendingRepositoriesUC: domainComponent.syncTrendingRepositoriesUC(),
                              observeTrendingRepositoriesUC: domainComponent.observeTrendingRepositoriesUC())
    }

    func repositoryDetailsViewModel(repositoryURL: String) -> RepositoryDetailsViewModel {
        RepositoryDetailsViewModel(repositoryURL: repositoryURL,
                                   observeRepositoryUC: domainComponent.observeRepositoryUC())
    }
}

let applicationComponent: ApplicationComponent = {
    let dataComponent = DataModule()
    let domainComponent = DomainModule(dataComponent: dataComponent)
    return ApplicationModule(dataComponent: dataComponent, domainComponent: domainComponent)
}()
