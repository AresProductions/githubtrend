//
//  GithubTrendApp.swift
//  GithubTrend
//
//  Created by Ares Ceka on 28/10/22.
//

import SwiftUI

@main
struct GithubTrendApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                TrendingListView()
            }
        }
    }
}
