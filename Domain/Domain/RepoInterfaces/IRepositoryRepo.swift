//
//  IRepositoryRepo.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation
import Combine

public protocol IRepositoryRepo {
    func observeTrending() -> AnyPublisher<[Repository], Never>
    func observe(url: String) -> AnyPublisher<Repository?, Never>
    func syncTrending() async -> Result<Void, DomainError>
}
