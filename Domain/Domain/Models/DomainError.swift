//
//  DomainError.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation

public enum DomainError: Error, Equatable {
    case basic(description: String)
    case remote(description: String)
    case local(description: String)
    case unknown(description: String)
}
