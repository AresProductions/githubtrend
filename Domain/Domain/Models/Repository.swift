//
//  Repository.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation

public struct Repository: Equatable {
    public let name: String
    public let author: String
    public let avatarImageURL: String
    public let url: String
    public let description: String
    public let language: String?
    public let languageColorHexCode: String?
    public let stars: Int
    public let forks: Int
    public let contributors: [Contributor]

    public init(
        name: String,
        author: String,
        avatarImageURL: String,
        url: String,
        description: String,
        language: String?,
        languageColorHexCode: String?,
        stars: Int,
        forks: Int,
        contributors: [Contributor]) {
        self.name = name
        self.author = author
        self.avatarImageURL = avatarImageURL
        self.url = url
        self.description = description
        self.language = language
        self.languageColorHexCode = languageColorHexCode
        self.stars = stars
        self.forks = forks
        self.contributors = contributors
    }
}
