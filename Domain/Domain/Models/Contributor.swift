//
//  Contributor.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation

public struct Contributor: Equatable {
    public let username: String
    public let profileURL: String
    public let avatarImageURL: String

    public init(username: String, profileURL: String, avatarImageURL: String) {
        self.username = username
        self.profileURL = profileURL
        self.avatarImageURL = avatarImageURL
    }
}
