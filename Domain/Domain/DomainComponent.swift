//
//  DomainComponent.swift
//  Domain
//
//  Created by Ares Ceka on 30/10/22.
//

import Foundation

public protocol DomainComponent {
    func observeTrendingRepositoriesUC() -> IObserveTrendingRepositoriesUC
    func syncTrendingRepositoriesUC() -> ISyncTrendingRepositoriesUC
    func observeRepositoryUC() -> IObserveRepositoryUC
}
