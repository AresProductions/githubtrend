//
//  ObserveTrendingRepositoriesUC.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation
import Combine

public protocol IObserveTrendingRepositoriesUC {
    func execute() -> AnyPublisher<[Repository], Never>
}

public class ObserveTrendingRepositoriesUC: IObserveTrendingRepositoriesUC {
    private let repo: IRepositoryRepo

    public init(repo: IRepositoryRepo) {
        self.repo = repo
    }

    public func execute() -> AnyPublisher<[Repository], Never> {
        return repo.observeTrending()
    }
}
