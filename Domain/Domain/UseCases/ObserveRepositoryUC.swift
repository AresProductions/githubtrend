//
//  ObserveRepositoryUC.swift
//  Domain
//
//  Created by Ares Ceka on 31/10/22.
//

import Foundation
import Combine

public protocol IObserveRepositoryUC {
    func execute(url: String) -> AnyPublisher<Repository?, Never>
}

public class ObserveRepositoryUC: IObserveRepositoryUC {
    private let repo: IRepositoryRepo

    public init(repo: IRepositoryRepo) {
        self.repo = repo
    }

    public func execute(url: String) -> AnyPublisher<Repository?, Never> {
        return repo.observe(url: url)
    }
}
