//
//  SyncTrendingRepositoriesUC.swift
//  Domain
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation
import Combine

public protocol ISyncTrendingRepositoriesUC {
    func execute() async -> Result<Void, DomainError>
}

public class SyncTrendingRepositoriesUC: ISyncTrendingRepositoriesUC {
    private let repo: IRepositoryRepo

    public init(repo: IRepositoryRepo) {
        self.repo = repo
    }

    public func execute() async -> Result<Void, DomainError> {
        return await repo.syncTrending()
    }
}
