//
//  ObserveTrendingRepositoriesUCTests.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Foundation
@testable import Domain
import XCTest
import Combine
import Nimble

class ObserveTrendingRepositoriesUCTests: XCTestCase {
    var cancellables = [AnyCancellable]()

    func testReturnsRepositoriesGivenRepoReturnsRepositories() {
        // given
        let repositories = [anyRepository(), anyRepository(), anyRepository()]
        let mockRepo = MockRepositoryRepo(expectedTrendingRepositories: repositories)
        let useCase = ObserveTrendingRepositoriesUC(repo: mockRepo)

        // when
        let expectation = XCTestExpectation(description: "received")
        var receivedRepositories: [Repository]?
        useCase.execute().sink { repositories in
            receivedRepositories = repositories
            expectation.fulfill()
        }.store(in: &cancellables)

        // then
        wait(for: [expectation], timeout: 3)
        expect(repositories).to(equal(receivedRepositories))
        expect(mockRepo.observeTrendingExecute.count).to(equal(1))
    }
}
