//
//  ObserveRepositoryUCTests.swift
//  DomainTests
//
//  Created by Ares Ceka on 31/10/22.
//

import Combine
@testable import Domain
import Foundation
import Nimble
import XCTest

class ObserveRepositoryUCTests: XCTestCase {
    var cancellables = [AnyCancellable]()

    func testReturnsRepositoryGivenRepoReturnsRepository() {
        // given
        let repository = anyRepository()
        let mockRepo = MockRepositoryRepo(expectedRepository: repository)
        let useCase = ObserveRepositoryUC(repo: mockRepo)

        // when
        let expectation = XCTestExpectation(description: "received")
        var receivedRepository: Repository?
        useCase.execute(url: repository.url).sink { repository in
            receivedRepository = repository
            expectation.fulfill()
        }.store(in: &cancellables)

        // then
        wait(for: [expectation], timeout: 3)
        expect(repository).to(equal(receivedRepository))
        expect(mockRepo.observeExecute.count).to(equal(1))
        expect(mockRepo.observeExecute[0]).to(equal(repository.url))
    }
}
