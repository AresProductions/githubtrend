//
//  SyncTrendingRepositoriesUCTests.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
@testable import Domain
import Foundation
import Nimble
import XCTest

// swiftlint:disable force_try
// swiftlint:disable force_cast
class SyncTrendingRepositoriesUCTests: XCTestCase {
    func testReturnsVoidGivenRepoReturnsVoid() async {
        // given
        let mockRepo = MockRepositoryRepo(expectedSyncResult: .success(()))
        let useCase = SyncTrendingRepositoriesUC(repo: mockRepo)

        // when
        let result: Void = try! await useCase.execute().get()

        // then
        await expect(result).to(beVoid())
        await expect(mockRepo.syncTrendingExecute.count).to(equal(1))
    }

    func testReturnsErrorGivenRepoReturnsError() async {
        // given
        let mockError = DomainError.basic(description: "error")
        let mockRepo = MockRepositoryRepo(expectedSyncResult: .failure(mockError))
        let useCase = SyncTrendingRepositoriesUC(repo: mockRepo)

        do {
            // when
            _ = try await useCase.execute().get()
            XCTFail("should throw error")
        } catch {
            // then
            let receivedError = error as! DomainError
            await expect(receivedError).to(equal(mockError))
            await expect(mockRepo.syncTrendingExecute.count).to(equal(1))
        }
    }
}
