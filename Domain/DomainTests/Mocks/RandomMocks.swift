//
//  RandomMocks.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

#if DEBUG

import Foundation

public func randomString(length: Int = 10) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0..<length).map { _ in letters.randomElement()! })
}

public func randomInt() -> Int {
    Int.random(in: Int.min..<Int.max)
}

#endif
