//
//  MockRepositoryRepo.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

import Combine
@testable import Domain
import Foundation

class MockRepositoryRepo: IRepositoryRepo {
    let expectedSyncResult: Result<Void, DomainError>
    let expectedTrendingRepositories: [Repository]
    let expectedRepository: Repository?

    var syncTrendingExecute: [Void] = []
    var observeTrendingExecute: [Void] = []
    var observeExecute: [String] = []

    init(
        expectedSyncResult: Result<Void, DomainError> = .success(()),
        expectedTrendingRepositories: [Repository] = [],
        expectedRepository: Repository? = nil) {
        self.expectedSyncResult = expectedSyncResult
        self.expectedTrendingRepositories = expectedTrendingRepositories
        self.expectedRepository = expectedRepository
    }

    func syncTrending() async -> Result<Void, DomainError> {
        syncTrendingExecute.append(())
        return expectedSyncResult
    }

    func observeTrending() -> AnyPublisher<[Repository], Never> {
        observeTrendingExecute.append(())
        return Just(expectedTrendingRepositories).eraseToAnyPublisher()
    }

    func observe(url: String) -> AnyPublisher<Repository?, Never> {
        observeExecute.append(url)
        return Just(expectedRepository).eraseToAnyPublisher()
    }
}
