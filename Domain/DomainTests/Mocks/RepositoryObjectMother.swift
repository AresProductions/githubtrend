//
//  RepositoryObjectMother.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

#if DEBUG

import Foundation

public func anyRepository(name: String = randomString()) -> Repository {
    Repository(name: name,
               author: randomString(),
               avatarImageURL: randomString(),
               url: randomString(),
               description: randomString(),
               language: randomString(),
               languageColorHexCode: randomString(),
               stars: randomInt(),
               forks: randomInt(),
               // contributors always come sorted from the local source
               contributors: [anyContributor(), anyContributor(), anyContributor()]
                   .sorted(by: { contributorA, contributorB in
                       contributorA.username < contributorB.username
                   }))
}

#endif
