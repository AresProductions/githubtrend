//
//  ContributorObjectMother.swift
//  DomainTests
//
//  Created by Ares Ceka on 28/10/22.
//

#if DEBUG

import Foundation

func anyContributor() -> Contributor {
    Contributor(username: randomString(), profileURL: randomString(), avatarImageURL: randomString())
}

#endif
