# GithubTrend iOS project

## Description
The project’s idea is to list trending projects from Github, tap on one of them, and show their details.

## Building - Running Project
1. Clone the git repository
2. Install [SwiftLint](https://github.com/realm/SwiftLint). I prefer using Homebrew to install.
3. Open the workspace project on Xcode and run.
4. For running screenshot tests, please use iPhone 14 Pro simulator

## Requirements

* iPhone or simulator with iOS > 15.0
* Xcode 13.0+

# Implementation
### Technologies
* Language: Swift
* Core Libraries: SwiftUI, Combine, CoreData, Kingfisher, SnapshotTesting, Nimble
* iOS Target: 15.0+

### Architecture
* General Architecture: [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
* Presentation Layer: MVI
* Domain Layer: 2 core Use Cases
  1. Sync Use Case: Retrieves - updates data from a remote source and saves it to the local source (using Async methods)
  2. Observe Use Case: Create an observable stream from the local source and get notified every time values change (using Publisher)
* Data Layer: Uses repository to make a connection between the different data sources (in our case Remote and Local sources)
  1. Local Source uses Core Data to save all necessary data for the app. It is the Single Source of Truth.
  2. Remote Source downloads data from the [Trending Github API](https://github-trending-api-wonder.herokuapp.com/)
  3. Repository will make the connection between the two sources while keeping the abstraction
* Testing: Unit and Screenshot Testing

### Changelog
* refactor: rename project (#13)
* fix: flaky screenshot tests on dark mode simulator (#12)
* docs: update README (#12)
* feat: add animation when switching state (#11)
* chore: use theme fonts on list and detail view (#10)
* feat: implement home view (#10)
* tests: add view model unit tests (#9)
* tests: add screenshot testing (#8)
* fix: edge cases when observing repositories (#7)
* refactor: add TrendingListView enum state (#7)
* feat: implement RepositoryDetailsView (#7)
* feat: implement TrendingListView (#7)
* feat: implement local data source (#6)
* feat: implement remote source (#5)
* feat: data layer repository implementation (#4)
* feat: Implement Domain Layer Sync and Observe Use Cases (#3)
* build: initial commit + swiftlint (#2)
